# Progetto "giterature" [TOP DOWN approach]

## ATTORI/RUOLI

* lettore/revisore/taggatore
* beta tester (caso particolare di lettore)
* autore
* editor
* editore
* moderatore


## USE CASE

1. *editore* commissiona a *author* un *testo*, assegna un *editor*
1. *beta tester* che legge un testo secondo uno specifico target
1. classe a scuola che analizza un testo esistente, in maniera collaborativa (anche CROWD!!! heat maps!!!)
1. classe a scuola (anche scuola di scrittura) che produce in maniera collaborativa un "tema"
1. tesi di laurea (mettiamo su un "tesificio a bitcoin" e facciamo un mucchio di soldi)
1. serial televisivo



## Sequence diagrams

## State diagrams

## Class...



---






# User stories (alla Agile ...)

(in più rispetto agli usecase ci sono i mock-up della UI)





---





# Implementazione (sempre alto livello)

* scegliere tipologia di interazione
	* web vs. local app
	* wysiwyg vs. build

* scegliere "strade implementative"
	* (from scratch)
	* customizzazione

* scegliere formato file / tipo di tagging
	* markdown (MD/RST)
	* markup (HTML/XML)
	* latex style
	* GoogleDocs comments
	* ODT comments
	* ...

* analisi strumenti (collab) da customizzare

	* web (svantaggi: non ha stato locale a meno di non poter fare un download)
		* CodiMD
		* Firepad
		* Etherpad
		* CryptPad
		* MediaWiki (no, cfr. mongodb dependency)
		* TikiWiki (si integra con etherpad lite)
		* XWiki + RtWysiwyg
		* (OnlyOffice)
		* (CollaboraOnline)
		* (ownCloud/nextCloud)

	* local (svantaggi: installazione locale, gestione connessioni con server)
		* gobby
		* visual studio code (con plugin)
		* emacs (con estensioni)





# Strumenti di editing collaborativo WEB

## CodiMD (https://demo.codimd.org/ e https://github.com/hackmdio/codimd)
* Caratteristiche +++
	* AGPL3
	* JavaScript
	* community+
	* modalità preview, forse (TODO verificare!!!) supporto markdown definiti da noi, autocompletion, aggiunta dialog o simili
	* integrabile con GIT???
	* estendibilità
	* DB/filesystem/git
	* requisiti installazione

TODO
* installare un'istanza di prova
* trovare persone su JS e CSS (sentire realitygaps)
* capire se si può estendere markdown e previewer


DOMANDE DA FARE
* prima leggere https://github.com/hackmdio/codimd/issues/926
* studiare anche markdown-it
* come estendere il markdown
* come modificare il preview (es: ToC in terza finestra)
* come hanno fatto a integrare gli altri linguaggi (UML, Graphviz, Nusica ...)? Noi vogliamo integrare il nostro
* è possibile lanciare un engine di rendering esterno (es LaTeX) e integrarlo nella visualizzazione di CodiMD?
* documentazione sull'API


## Da valutare
* [Dillinger](https://github.com/joemccann/dillinger)
* [rassegna di editor](https://juretriglav.si/open-source-collaborative-text-editors/)
* [Pundit, web app per l'annotazione semantica](https://thepund.it/) (prodotto da una società pisana partner del network DARIAH)



## Scartati
### Firepad
* Caratteristiche
	* sort of public domain
	* JavaScript
	* community+
	* wysiwyg

### Etherpad (https://etherpad.org/)
* Caratteristiche ---
	* Apache 2
	* JavaScript
	* community+
	* no preview solo wysiwyg (sì non ha edit in markdown mode), ?supporto markdown definiti da noi, no autocompletion, aggiunta dialog o similihttps://github.com/hackmdio/codimd)

### CryptPad (https://cryptpad.fr/)
* Caratteristiche --
	* AGPL3
	* JavaScript
	* <community>
	* no preview, ?supporto markdown definiti da noi, no autocompletion, ?aggiunta dialog o simili
	* francese
