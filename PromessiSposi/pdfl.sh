#!/bin/bash

if
 test $# -eq 0
then
 echo serve parametro
 exit
fi

pulisci() {
 rm errors.txt *.bb $1.out $1.aux $1.bbl $1.dvi $1.blg $1.toc $1.lof $1.log $1.nav $1.snm $1.xtr $1.g* $1.tdo $1.bcf $1.xml $1.tmp ${1}*latexmk $1.fff $1.fls
}


pandocbib(){
	for bib in *bib
	do
	 echo -e '--bibliography' $bib
	done
}

main()
{
pulisci $1

#echo TOLTA copia dei bib!
#cp ../../../AT_*.bib .

# più volte per embedded, una sola per articolo shus (ad esempio), MAH!
for volte in 1 2 3
do
    nice latexmk -pdf -pdflatex="xelatex"  -bibtex $1.tex
	#latexmk  -pdf -pdflatex="pdflatex" -bibtex $1.tex
    #latexmk -pdf $1.tex

    #pdflatex $1.tex

	nice makeglossaries $1

	#bibtex $1

	#latexmk -pdf $1.tex
done

#for volte in 1 2
#do
#	#pdflatex $1
#	xelatex $1
#	bibtex $1
#	#biber $1
#	makeglossaries $1
#	#xindy $1
#done


#####NON USARE!!!#################################
#echo '***-RTF-----------------------------------'
##latex2rtf $1
#pandoc $1.tex -w rtf -o $1-2.rtf
##################################################
#echo '***-ODT------------------------------------'
#pandoc $1.tex -t odt -o $1.odt
##################################################
#echo '***-TXT------------------------------------'
#pandoc $1.tex -t plain -o $1.text
##################################################
#echo '***-DOCX-----------------------------------'
#pandoc $1.tex -t docx $(pandocbib) -o $1.docx
##################################################
#echo '***-HTML----------------------------------'
#latex2html -split 0 $1
#tex4ht $1
#mk4ht oolatex $1
#htlatex $1
##################################################
#echo '***-DETEX---------------------------------'
#detex $1 > $1.detexed
##################################################
#echo '***-EPUB-----------------------------------'
#latexml --dest=$1.xml $1.tex
#latexmlpost --novalidate -dest=$1.html $1.xml
#ebook-convert $1.html $1.epub --language en --no-default-epub-cover
##################################################

#kpdf $1.pdf

#pulisci $1
}

##################################################
main $1 2>&1  |tee pdfl.log  

##################################################
##echo -n '===>>> conteggio caratteri: '
##pdftotext $1.pdf -enc UTF-8 - |wc -m
##texcount -char -inc $1.tex > texcount.log
texcount -col -merge -incbib $1.tex -out=texcount.stats
texcount -col -merge -incbib $1.tex -html -out=texcount.html

evince $1.pdf &
