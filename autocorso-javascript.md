# Auto-Corso JavaScript

# Fonti

* W3Schools
* https://eu.udacity.com/course/intro-to-javascript--ud803

## Scopi

* imparare javascript
* realizzare PoC di rappresentatore semantico di un testo
	* un plugin?
	* modulare
	* che utilizza un set di tag configurabile
	* libero (ca va sans dire!)


## Cose che potrebbero essere utili o cmq da studiare

* framework vari classici
	* React
	* Angular
	* Bootstrap
	* jQuery
* D3 (e successive)
