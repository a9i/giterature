# Appunti progetto "giterature" [BOOTOM UP approach/notes]

Scopo: piattaforma per "editing" collaborativo di testi molto strutturati

METAFORA FORTE: concetto di "dashboard" alla Scrivener ma MOOOOOOLTO meglio! (vedi napkin)

cfr.
* https://www.researchgate.net/publication/268195980_Ontology-Based_Visualization_of_Characters%27_Intentions/figures
* https://www.literatureandlatte.com/scrivener/features
* https://www.literatureandlatte.com/assets/image/scrivener/features/macOS/4-outliner-1.jpg

Target: utenti avanzati, scrittori molto consapevoli (soprattutto rispetto alle strutture e ai template dei vari tipi di letteratura, "flusso di coscienza" no grazie)

Specifico:

* set di tool "programmativi" per costruire iperstorie (più piani, aka film "vantage point" o "no country for old men")
* estrazione "schemi" (personaggi, situazioni, luoghi, struttura)
* controllo di compliance "contro" un template



# Cosa mi/ci piacerebbe avere da subito o quasi

* strumenti di estrazione/schematizzazione da usare:
    * sia a priori (pianificazione dell'opera)
    * che in itinere (steering)
    * che a posteriori (compliance, marketing/vendita)



# Esempio immaginifico

Creo un testo, patendo dall'outline, che strutturo secondo un template di opera che ho in mente (magari per motivi commerciali, vendibilità del momento), quindi pre-taggo i capitoli e le sezioni secondo il template.
Poi riempio di testo, taggando opportunamente e/o inserendo pezzi di testo "di markup" (alla latex/beamer o latex/extract o latex/todonotes)
Man mano ho uno strumento che mi genera "statistiche on steroids" (alla gitstats come tipologia di workflow, non come tipo di output), estraendo schemi, elenchi di tag, etc.
Meglio ancora se fornisse KPI (Key Perfomance Indicators) misurati "against" i template, es. questo testo è al 60% un romanzo d'amore e al 22% un giallo,...

Indici vari (leggibilità, per minori, etc.)

Altra metafora che si può usare è quella del testing nel sw



# TODO in itinere

* actors and roles, and use cases i.e. progettazione del processo
* competitors (es. TYPEE, atlas.oreilly.com, draftin.com,...)
* brand



# TODO NOW

* provare a scrivere un pezzo sostanzioso (o prendere un testo già fatto), cercando package latex che aiutino fin da subito a dare un'idea del prodotto finale

Manuzio o https://www.gutenberg.org

http://www.george-orwell.org/1984/0.html


# COSE CHE VOGLIAMO ESTRARRE / PRESENTARE AD UN PUBBLICO / SHOW OFF

Pensare a "cosi" che fanno bella figura, da mostrare in una presentazione ad un Venture Capital


* index dei tag, con nr pagina (su un doc e su un file + strutturato, tipo CSV?)
	[cercare package che generano dataset]
	[cercare package per annotations]

* wordcloud dei tag

* "grafo" dei tag
	esempio: interazioni tra personaggi


# Cose da studiare (sia per ispirarsi che per non "reinvent the wheel")

* TEI: Text Encoding Initiative
    * [sito ufficiale](https://tei-c.org/)
    * [TEI su Wikipedia](https://en.wikipedia.org/wiki/Text_Encoding_Initiative)  
    * [TAPAS](http://www.tapasproject.org/) - TEI Archiving, Publishing, and Access Service
* DARIAH: The Digital Research Infrastructure for the Arts and Humanities
    * [sito ufficiale](https://www.dariah.eu/)
    * [la piattaforma di e-learning](https://teach.dariah.eu/)
    * [il portale italiano](http://it.dariah.eu/)
* dublin core, RDF
    * https://www.w3.org/wiki/Lists_of_ontologies
    * https://www.researchgate.net/publication/268195980_Ontology-Based_Visualization_of_Characters%27_Intentions
    * https://philpapers.org/rec/ECOOTO
* biblioteche e classificazione Dewey
* pacchetti LaTeX
* diritto d'autore
* Xpath, (xslt) e tool stile xsh (https://metacpan.org/pod/distribution/XML-XSH2/xsh)
* awk, sempre per estrarre
* editor XML con tag configurabili (pennello e tag)
* javascript infarinatura (via MOOC o https://www.w3schools.com), in prospettiva per anche solo capire come modificare un codimd qualunque o anche (+ interessante) per elaborare data driven documents con D3 e simili (es. un grafico semantico di un testo)


# In prospettiva

* analisi del'effetto sui lettori
* sentiment analysis sui singoli pezzi
* crowd tagging
* Interactive Fiction
    * [articolo](https://en.wikipedia.org/wiki/Interactive_fiction) su Wikipedia
    * [Dialog](http://www.linusakesson.net/dialog/index.php), un DSL per scrivere IF



# Business model

* piattaforma sw libera
* contenuti, ogni autore sceglie la propria licenza (alla gitlab)
* incentive/rewarding model per "orientare" le scelte sulle licenze
* sponsorship, prizes
* fondi/finanziamenti/bandi...



# Ipotesi sullo strumento

(siamo greedy)

* qualunque cosa è taggabile/commentabile/votabile, anche la singola parola o carattere (NON alla TYPEE in cui si vota il racconto o alla FB in cui si "likea" il post, ma si deve poter "intertwinare" cose su cose), soprattutto "crowd" perché si può dare più peso ad una cosa segnalata da molti utenti (stile "autovelox su strada")
	esempio: "perché hai messo un punto esclamativo proprio qui?"
	
* lo strumento serve sia per valutare/aiutare l'autore (che viene "commentato" quindi analizzato da un crowd), ma anche (a regime, servono numeri grossi) per valutare l'efficacia (reputation) dei taggatori
	cfr. recommending systems, fake reviewer vs. fair reviewer
	
* si deve poter estrarre in maniera programmativa ogni tipo di informazione, ergo, tagging facilmente parsabile e con semantica definita (non assoluta, ma deve essere possibile dare una semantica dinamica)
	esempio: il tag #faseDelViaggioDellEroe me lo definisco come voglio io
	(alla "linguaggio di programmazione" in cui posso definire "funzioni")
	
* potendo estrarre info posso creare sia meta-documenti che "spieghino" il documento in oggetto (es. sinossi, wordcloud, fasi del racconto, etc.) sia analisi sul testo (stile del romanzo, target di lettori, etc.)

* la parola che si può usare è "instrumenting text" (cfr. aspect programming)

* siamo onesti: sottrentanni che si parla di testo e meta-testo (vedi SGML, HTML, etc.) ma forse finalmente possiamo spingere per un pensiero più struttural-progettista di un testo
	elaboro: vogliamo finalmente passare da "bold/underline/indent" a "capitolo/sezione" (e c'è in qualunque strumento serio come LaTeX) e soprattutto a "introduzione/crisi/chiamata/mentore"?!!!

* deeplearning (fa molto google), forse

* analisi del testo per prima sgrossatura e pre-tagging

* metafora di "scratch" (scratch.mit.edu): linguaggio sottostante, building blocks per utonti


* internazionalizzazione










# PROVE markdown

digraph graphname {
		T [label="Teacher" color=Blue, fontcolor=Red, fontsize=24, shape=box]      // node T
		P [label="Pupil" color=Blue, fontcolor=Red, fontsize=24, shape=box]  // node P

		T->P [label="Instructions", fontcolor=darkgreen] // edge T->P
}

Title: Here is a title
A->B: Normal line
B-->C: Dashed line
C->>D: Open arrow
D-->>A: Dashed open arrow










# Strati del processo

Dal basso verso l'alto

* rappresentazione della "strisciata" dei tag, viewer
	- latex + packages di visualizzazione (es. tikz)
	- dot files
	- kml per i luoghi
	- formati specifici per contesti molto verticali (es. alberi genealogici, grafi)
* estrazione/filtro dei tag
	- DOM/streaming parsers
	- unix-like tools (grep/etc.)
* tagging overlay
	- latex + pacchetti vari
	- md/rst/pandoc
	- xml/html-like
	- word comments (problema estrazione)
	- googledocs comments (problema estrazione)
* testo raw
	- scrivi/importi





# Parametrizzazione del processo

* definire "alfabeto" testo e meta-testo "RAW"
* quindi definire cosa è taggabile
* definire alfabeto del tagging
* e il sistema di coordinate

A questo punto si può pensare di estrarre una lista di tuple:

	(posizione, testo taggato, stringa del tag)


Ci sono casi in cui si potrebbe voler taggare ad esempio l'uso del grassetto, ergo non può essere ignorato il "meta testo grassetto" o altra formattazione.

Potremmo voler taggare un'immagine, o testo scritto non sequenzialmente (es. poesie futuriste).

Potremmo voler usare come sistema di coordinate la numerazione in capitoli, sezioni, pagine, etc.


# Esempi

(a meno delle coordinate posizionali del testo taggato)

## Stile markup (comporta creazione di tool di visualizzazione ed estrazione, da zero)
* testo normale è ascii
* meta testo è incluso in '<tagXXX testotag>' e '</tagXXX>' (stile html)
* estrazione genera:
	tagXXX testotag...
	tagYYY testotag...
	
## Stile LaTeX ()
* testo normale è ascii escluso ciò che sta dentro comandi latex
* meta testo è incluso in '\luogo{testotag}{' e '}'
* estrazione genera:
	tagXXX testotag...
	tagYYY testotag...


# Incontro del 04.04.2019

Scenario di utilizzo nelle scuole: serve un tool collaborativo interattivo?
Collaborativo sì, anche interattivo, ma non necessariamente subito.

Valutare:
* Google Docs (API)
* LibreOffice online (https://www.collaboraoffice.com/code/)
* Visual Studio Code (https://code.visualstudio.com/)









# Links

https://pad.libreho.st/nftABCKTSYu1VmD5bku3UA?both


testo da taggare:
https://it.wikisource.org/wiki/I_promessi_sposi_(1840)



metadata repositories/registries
https://en.wikipedia.org/wiki/Metadata_registry#Examples_of_public_metadata_registries


piattaforme con cui confrontarsi
* https://leanpub.com
* https://www.storyboardthat.com/it (solo per i target: scuole, aziente, industria cinematografica)






# Frameworks per realizzare collaborative applications/websites/etc.

## key koncepts

* CRDT https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type
* OT https://en.wikipedia.org/wiki/Operational_transformation


## frameworks

Pagine interessanti:

* https://www.quora.com/What-are-good-frameworks-for-real-time-collaboration-in-a-web-application
* https://irisate.com/collaborative-editing-solutions-round-up/

ci sono sia dei semplici DB collaborativi sia interi framework più ad alto livello

* Peerpad http://peerpad.net/
* TogetherJS https://togetherjs.com/
* ShareJS https://github.com/josephg/ShareJS
